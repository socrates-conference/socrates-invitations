CREATE TABLE `invitations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inviter_person_id` INT(11) NOT NULL,
  `inviter_email` varchar(100) NOT NULL,
  `invitee1_person_id` INT(11) NOT NULL,
  `invitee1_email` varchar(100) NOT NULL,
  `invitee2_person_id` INT(11) NOT NULL,
  `invitee2_email` varchar(100) NOT NULL,
  `status` varchar(24) DEFAULT 'OPEN',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;