//@flow

import type {Invitation, InvitationStatus} from './invitations';
import MySql from '../db/MySql';

export interface InvitationsRepository {
  find(id: number): Promise<?Invitation>;

  findAll(): Promise<Array<Invitation>>;

  update(update: Invitation): Promise<Invitation>;

  create(invitation: Invitation): Promise<Invitation>;
}

type InsertResult = {error: Error} | {numberOfRowsAffected: number, lastInsertedId: number}
type UpdateResult = {error: Error} | {numberOfRowsAffected: number}

type InvitationsEntry = {
  id: number,
  inviterId: number,
  inviterEmail: string,
  invitee1Id: number,
  invitee1Email: string,
  invitee2Id: number,
  invitee2Email: string,
  status: InvitationStatus
};

function invitationsEntryToInvitation(selected: InvitationsEntry): Invitation {
  return {
    id: selected.id, status: selected.status,
    inviter: {id: selected.inviterId, email: selected.inviterEmail},
    invitee1: {id: selected.invitee1Id, email: selected.invitee1Email},
    invitee2: {id: selected.invitee2Id, email: selected.invitee2Email}
  };
}

export default class InvitationsMySqlRepository implements InvitationsRepository {
  _mysql: MySql;

  constructor(mysql: MySql) {
    this._mysql = mysql;
  }

  async find(id: number): Promise<?Invitation> {
    const selectStatement = 'SELECT inviter_person_id AS inviterId, inviter_email AS inviterEmail,' +
      'invitee1_person_id AS invitee1Id, invitee1_email AS invitee1Email,' +
      'invitee2_person_id AS invitee2Id, invitee2_email AS invitee2Email,' +
      'status, id from invitations WHERE id=?';
    const selected = await this._mysql.selectWithParams(selectStatement, [id]);
    return invitationsEntryToInvitation(selected[0]);
  }

  async findAll(): Promise<Array<Invitation>> {
    const selectStatement = 'SELECT inviter_person_id AS inviterId, inviter_email AS inviterEmail,' +
      'invitee1_person_id AS invitee1Id, invitee1_email AS invitee1Email,' +
      'invitee2_person_id AS invitee2Id, invitee2_email AS invitee2Email,' +
      'status, id from invitations';
    const selected = await this._mysql.select(selectStatement);
    return selected.map(invitationsEntryToInvitation);
  }

  async update(update: Invitation): Promise<Invitation> {
    const values = 'inviter_person_id=?, inviter_email=?, ' +
      'invitee1_person_id=?, invitee1_email=?, ' +
      'invitee2_person_id=?, invitee2_email=?, ' +
      'status=?';
    const updateStatement = `UPDATE invitations SET ${values} WHERE id=?`;
    const {
      inviter: {id: inviterId, email: inviterEmail},
      invitee1: {id: invitee1Id, email: invitee1Email},
      invitee2: {id: invitee2Id, email: invitee2Email},
      status, id
    } = update;
    const queryResult: UpdateResult = await this._mysql
      .insert(updateStatement,
        [inviterId, inviterEmail,
          invitee1Id, invitee1Email,
          invitee2Id, invitee2Email, status, id])
      .catch(error => ({error}));

    if (queryResult.error) {
      throw new Error(queryResult.error);
    }

    return {...update};
  }

  async create(invitation: Invitation): Promise<Invitation> {
    const fieldNames = 'inviter_person_id, inviter_email, ' +
      'invitee1_person_id, invitee1_email, ' +
      'invitee2_person_id, invitee2_email, ' +
      'status';
    const insertStatement = `INSERT INTO invitations (${fieldNames}) VALUES (?,?,?,?,?,?,?)`;
    const {
      inviter: {id: inviterId, email: inviterEmail},
      invitee1: {id: invitee1Id, email: invitee1Email},
      invitee2: {id: invitee2Id, email: invitee2Email},
      status
    } = invitation;

    const queryResult: InsertResult = await this._mysql
      .insert(insertStatement,
        [inviterId, inviterEmail,
          invitee1Id, invitee1Email,
          invitee2Id, invitee2Email, status])
      .catch(error => ({error}));

    if (queryResult.error) {
      throw new Error(queryResult.error);
    }

    const id: number = queryResult.lastInsertedId;
    return {...invitation, id};
  }
}

export class InvitationsTestRepository implements InvitationsRepository {
  _count: number;
  _invitations: Map<number, Invitation>;

  constructor() {
    this._count = 0;
    this._invitations = new Map();
  }

  async find(id: number): Promise<?Invitation> {
    return this._invitations.get(id);
  }

  async findAll(): Promise<Array<Invitation>> {
    return [...this._invitations.values()];
  }

  async update(update: Invitation): Promise<Invitation> {
    this._invitations.set(update.id, update);
    return update;
  }

  async create(invitation: Invitation): Promise<Invitation> {
    const id = ++this._count;
    const stored = {...invitation, id};
    this._invitations.set(id, stored);
    return stored;
  }
}