// @flow

import * as express from 'express';
import Invitations from '../invitations';
import {tryToProcess, throwServerError} from 'server-commons';
import type {EventDispatcher} from 'server-commons/src/domain/eventDispatcher';

export default class InvitationsApiVersion1 extends express.Router {
  _invitations: Invitations;
  _dispatcher: EventDispatcher;

  constructor(invitations: Invitations, dispatcher: EventDispatcher) {
    super();
    this._invitations = invitations;
    this._dispatcher = dispatcher;
    this._initRoutes();
  }

  _initRoutes = () => {
    this.get('/invitations', async (req: express.Request, res: express.Response) => {
      const invitations = await this._invitations.list();
      res.status(200).send(invitations).end();
    });

    this.post('/invitations', async (req: express.Request, res: express.Response) => {
      await tryToProcess(req, res, invitation => {
        const event = {type: 'SEND_INVITATION', payload: invitation};
        this._dispatcher.dispatchEvent(event);
        return Promise.resolve();
      });
    });

    this.put('/invitations/:id', async (req: express.Request, res: express.Response) => {
      await tryToProcess(req, res, async () => {
        if (!req.query) {
          return throwServerError(400, 'Missing parameters');
        } else if (!req.query.sender) {
          return throwServerError(400, 'Missing sender');
        } else if (!req.query.action) {
          return throwServerError(400, 'Missing action');
        } else if (req.query.action === 'accept') {
          const payload = {id: parseInt(req.params.id), sender: req.query.sender};
          this._dispatcher.dispatchEvent({type: 'ACCEPT_INVITATION', payload});
          return Promise.resolve();
        } else if (req.query.action === 'decline') {
          const payload = {id: parseInt(req.params.id), sender: req.query.sender};
          this._dispatcher.dispatchEvent({type: 'DECLINE_INVITATION', payload});
          return Promise.resolve();
        } else {
          return throwServerError(400, 'Invalid action: %s', req.query.action);
        }
      });
    });
  };
}