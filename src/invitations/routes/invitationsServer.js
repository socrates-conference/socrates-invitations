// @flow

import express from 'express';
import type {Middleware} from 'server-commons/src/middleware';
import type {EventDispatcher} from 'server-commons/src/domain/eventDispatcher';
import InvitationsApiVersion1 from './invitationsApiVersion1';
import Invitations from '../invitations';

export default function createInvitationsServer(invitations: Invitations, dispatcher: EventDispatcher): Middleware {
  return (app: express.Application) => app.use('/api/v1/',
    new InvitationsApiVersion1(invitations, dispatcher));
}