// @flow

import type {EventDispatcher} from 'server-commons';
import {Aggregate} from 'server-commons';
import type {Logger} from 'server-commons/src/logger';
import {InvitationsRepository} from './invitationsRepository';

export type Person = {
  id: number,
  email: string
}

export type Invitation = {
  id: number,
  invitee1: Person,
  invitee2: Person,
  inviter: Person,
  status: InvitationStatus
}

export type InvitationStatus =
  'UNKNOWN'
  | 'OPEN'
  | 'ACCEPTED'
  | 'DECLINED'
  | 'PARTIALLY_ACCEPTED:1'
  | 'PARTIALLY_ACCEPTED:2';

const isDirectInvitation = (inv: Invitation) => inv.invitee1.id === inv.inviter.id;
const isInvitee1 = (inv: Invitation, sender) => inv.invitee1.email === sender;
const isInvitee2 = (inv: Invitation, sender) => inv.invitee2.email === sender;
const inviteeAccepted = (invitation: Invitation, n: number) => invitation.status.endsWith(String(n));
const isOtherInvitee = (invitation: Invitation, sender) =>
  (isInvitee1(invitation, sender) && inviteeAccepted(invitation, 2)) ||
  (isInvitee2(invitation, sender) && inviteeAccepted(invitation, 1));

const validateForAccept = (invitation, id, sender) => {
  if (!invitation) {
    throw new Error('Invitation cannot be accepted: No such invitation: ' + id);
  }
  if (!(isInvitee1(invitation, sender) || isInvitee2(invitation, sender))) {
    throw new Error('Invitation cannot be accepted: Sender does not match any invitees.');
  }
  return invitation;
};

const validateForDecline = (invitation: ?Invitation, id: number, sender: string) => {
  if (!invitation) {
    throw new Error('Invitation cannot be declined: No such invitation: ' + id);
  }
  if (!(isInvitee1(invitation, sender) || isInvitee2(invitation, sender))) {
    throw new Error('Invitation cannot be declined: Sender does not match any invitees.');
  }
  return invitation;
};

export default class Invitations extends Aggregate {
  _logger: Logger;
  _invitations: Map<number, Invitation>;
  _repository: InvitationsRepository;

  constructor(repository: InvitationsRepository, dispatcher: EventDispatcher, logger: Logger) {
    super(dispatcher);
    this._logger = logger;
    this._invitations = new Map();
    this._repository = repository;
  }

  async accept(id: number, sender: string) {
    const stored = await this._repository.find(id);
    const invitation = validateForAccept(stored, id, sender);

    let status;
    switch (invitation.status) {
      case 'OPEN':
        status = isInvitee2(invitation, sender)
          ? isDirectInvitation(invitation) ? 'ACCEPTED' : 'PARTIALLY_ACCEPTED:2'
          : 'PARTIALLY_ACCEPTED:1';
        break;
      case 'PARTIALLY_ACCEPTED:1':
      case 'PARTIALLY_ACCEPTED:2':
        if (isOtherInvitee(invitation, sender)) {
          status = 'ACCEPTED';
        }
        break;
      default:
        this._logger.error('Cannot accept invitation for %s at status %s', sender, invitation.status);
        throw new Error('Invitation cannot be accepted: Invalid action at current status.');
    }
    const next = {...invitation, status};
    await this._repository.update(next);

    if (status === 'ACCEPTED') {
      this._dispatcher.dispatchEvent({type: 'INVITATION_ACCEPTED', payload: next});
    } else if (status && status.startsWith('PARTIALLY')) {
      this._dispatcher.dispatchEvent({type: 'INVITATION_PARTIALLY_ACCEPTED', payload: next});
    }
  }

  async decline(id: number, sender: string): Promise<void> {
    const stored = await this._repository.find(id);
    const invitation = validateForDecline(stored, id, sender);

    if (invitation.status !== 'ACCEPTED' && invitation.status !== 'DECLINED') {
      const status = 'DECLINED';
      const next = {...invitation, status};
      await this._repository.update(next);
      this._dispatcher.dispatchEvent({type: 'INVITATION_DECLINED', payload: next});
    }
  }

  async send(invitation: Invitation): Promise<Invitation> {
    invitation.status = 'OPEN';
    const stored = await this._repository.create(invitation);
    this._dispatcher.dispatchEvent({type: 'INVITATION_SENT', payload: stored});
    return stored;
  }

  async list(): Promise<Invitation[]> {
    return this._repository.findAll();
  }
}
