// @flow

import {Event, DomainEventDispatcher, testLogger} from 'server-commons';
import Invitations from './invitations';
import {InvitationsTestRepository} from './invitationsRepository';

const invitation = {
  id: 0,
  inviter: {id: 1, email: 'test@test.de'},
  invitee1: {id: 1, email: 'test@test.de'},
  invitee2: {id: 2, email: 'test2@test.de'},
  status: 'UNKNOWN'
};
const acceptedDirectInvitation = {
  id: 1,
  inviter: {id: 1, email: 'test@test.de'},
  invitee1: {id: 1, email: 'test@test.de'},
  invitee2: {id: 2, email: 'test2@test.de'},
  status: 'ACCEPTED'
};
const declinedDirectInvitation = {
  id: 1,
  inviter: {id: 1, email: 'test@test.de'},
  invitee1: {id: 1, email: 'test@test.de'},
  invitee2: {id: 2, email: 'test2@test.de'},
  status: 'DECLINED'
};
const partiallyAcceptedInvitation = {
  id: 1,
  inviter: {id: 0, email: 'admin@test.de'},
  invitee1: {id: 1, email: 'test@test.de'},
  invitee2: {id: 2, email: 'test2@test.de'},
  status: 'PARTIALLY_ACCEPTED:2'
};
const acceptedAdminInvitation = {
  id: 1,
  inviter: {id: 0, email: 'admin@test.de'},
  invitee1: {id: 1, email: 'test@test.de'},
  invitee2: {id: 2, email: 'test2@test.de'},
  status: 'ACCEPTED'
};
const declinedAdminInvitation = {
  id: 1,
  inviter: {id: 0, email: 'admin@test.de'},
  invitee1: {id: 1, email: 'test@test.de'},
  invitee2: {id: 2, email: 'test2@test.de'},
  status: 'DECLINED'
};

describe('Invitations:', () => {
  const domainEventDispatcher = new DomainEventDispatcher();
  let invitations, events;
  const listener = (event: Event) => events.push(event);

  beforeEach(() => {
    events = [];
    invitations = new Invitations(new InvitationsTestRepository(), domainEventDispatcher, testLogger);
    domainEventDispatcher.subscribe('*', listener);
  });

  afterEach(() => {
    domainEventDispatcher.unsubscribe('*', listener);
  });

  describe('when sending an invitation', () => {
    let list, stored;
    beforeEach(async () => {
      stored = await invitations.send(invitation);
      list = await invitations.list();

    });

    it('should contain 1 open invitation', () => {
      expect(list).toHaveLength(1);
      expect(list[0]).toEqual(stored);
    });

    it('should change invitation status to OPEN', () => {
      expect(list[0].status).toEqual('OPEN');
    });

    it('should dispatch INVITATION_SENT event', () => {
      expect(events[0].type).toEqual('INVITATION_SENT');
      expect(events[0].payload).toEqual(stored);
    });
  });

  describe('Given inviter is an attendee', () => {
    describe('and sender is the other invitee', () => {
      const sender = 'test2@test.de';

      describe('when accepting an invitation', () => {
        let list, stored;

        beforeEach(async () => {
          stored = await invitations.send(invitation);
          await invitations.accept(1, sender);
          list = await invitations.list();
        });

        it('should contain the accepted invitation', () => {
          expect(list).toHaveLength(1);
          expect(list[0].id).toEqual(stored.id);
        });

        it('should change invitation status to OPEN', () => {
          expect(list[0].status).toEqual('ACCEPTED');
        });

        it('should dispatch INVITATION_ACCEPTED event', () => {
          expect(events[1].type).toEqual('INVITATION_ACCEPTED');
          expect(events[1].payload).toEqual(acceptedDirectInvitation);
        });
      });

      describe('when trying to accept an unknown invitation', () => {
        it('should throw an error', done => {
          invitations.send(invitation)
            .then(() => invitations.accept(10, sender))
            // $FlowFixMe fail() should be recognized, it is part of jest
            .then(() => fail('Accepting an invitation should throw an error, when the invitation cannot be found'))
            .catch(() => done());
        });
      });

      describe('when trying to accept an invitation with status ACCEPTED', () => {
        it('should throw an error', done => {
          invitations.send({...invitation})
            .then(() => invitations.accept(1, sender))
            .then(() => invitations.accept(1, sender))
            // $FlowFixMe fail() should be recognized, it is part of jest
            .then(() => fail('Accepting an invitation should throw an error, ' +
              'when the invitation is already accepted'))
            .catch(() => done());
        });
      });

      describe('when declining an invitation', () => {
        let list, stored;

        beforeEach(async () => {
          stored = await invitations.send(invitation);
          await invitations.decline(1, sender);
          list = await invitations.list();
        });

        it('should contain the declined invitation', () => {
          expect(list).toHaveLength(1);
          expect(list[0].id).toEqual(stored.id);
        });

        it('should change invitation status to DECLINED', () => {
          expect(list[0].status).toEqual('DECLINED');
        });

        it('should dispatch INVITATION_DECLINED event', () => {
          expect(events[1].type).toEqual('INVITATION_DECLINED');
          expect(events[1].payload).toEqual(declinedDirectInvitation);
        });
      });

      describe('when trying to decline an unknown invitation', () => {
        it('should throw an error', done => {
          invitations.send(invitation)
            .then(() => invitations.decline(10, sender))
            // $FlowFixMe fail() should be recognized, it is part of jest
            .then(() => fail('Declining an invitation should throw an error, when the invitation cannot be found'))
            .catch(() => done());
        });
      });
    });

    describe('and sender is not an invitee', () => {
      const sender = 'unknown@test.de';
      describe('when accepting an invitation', () => {
        it('should throw an error', done => {
          invitations.send(invitation)
            .then(() => invitations.accept(1, sender))
            // $FlowFixMe fail() should be recognized, it is part of jest
            .then(() => fail('Accepting an invitation should throw an error when the sender is unknown'))
            .catch(() => done());
        });
      });

      describe('when declining an invitation', () => {
        it('should throw an error', done => {
          invitations.send(invitation)
            .then(() => invitations.decline(1, sender))
            // $FlowFixMe fail() should be recognized, it is part of jest
            .then(() => fail('Declining an invitation should throw an error when the sender is unknown'))
            .catch(() => done());
        });
      });
    });
  });

  describe('Given inviter is an admin', () => {

    beforeEach(() => {
      invitation.inviter.id = 0;
      invitation.inviter.email = 'admin@test.de';
    });

    describe('and sender is not an invitee', () => {
      const sender = 'unknown@test.de';

      describe('when accepting an invitation', () => {
        it('should throw an error', async (done) => {
          invitations.send(invitation)
            .then(() => invitations.accept(1, sender))
            // $FlowFixMe fail() should be recognized, it is part of jest
            .then(() => fail('Accepting an invitation should throw an error when the sender is unknown'))
            .catch(() => done());
        });
      });

      describe('when declining an invitation', () => {
        it('should throw an error', async (done) => {
          invitations.send(invitation)
            .then(() => invitations.decline(1, sender))
            // $FlowFixMe fail() should be recognized, it is part of jest
            .then(() => fail('Declining an invitation should throw an error when the sender is unknown'))
            .catch(() => done());
        });
      });
    });

    describe('and sender is the first invitee to respond', () => {
      const sender = 'test2@test.de';

      describe('when accepting an invitation', () => {
        let list, stored;

        beforeEach(async () => {
          stored = await invitations.send(invitation);
          await invitations.accept(1, sender);
          list = await invitations.list();
        });

        it('should contain the partially accepted invitation', () => {
          expect(list).toHaveLength(1);
          expect(list[0].id).toEqual(stored.id);
        });

        it('should change invitation status to PARTIALLY_ACCEPTED:{inviteeNumber}', () => {
          expect(list[0].status).toEqual('PARTIALLY_ACCEPTED:2');
        });

        it('should dispatch INVITATION_PARTIALLY_ACCEPTED event', () => {
          expect(events[1].type).toEqual('INVITATION_PARTIALLY_ACCEPTED');
          expect(events[1].payload).toEqual(partiallyAcceptedInvitation);
        });
      });

      describe('when declining an invitation', () => {
        let list, stored;

        beforeEach(async () => {
          stored = await invitations.send(invitation);
          await invitations.decline(1, sender);
          list = await invitations.list();
        });

        it('should contain the declined invitation', () => {
          expect(list).toHaveLength(1);
          expect(list[0].id).toEqual(stored.id);
        });

        it('should change invitation status to DECLINED', () => {
          expect(list[0].status).toEqual('DECLINED');
        });

        it('should dispatch INVITATION_DECLINED event', () => {
          expect(events[1].type).toEqual('INVITATION_DECLINED');
          expect(events[1].payload).toEqual(declinedAdminInvitation);
        });
      });
    });
    describe('and sender is the second invitee to respond', () => {
      const sender = 'test@test.de';

      describe('when accepting an invitation', () => {
        let list, stored;

        beforeEach(async () => {
          stored = await invitations.send(invitation);
          await invitations.accept(1, 'test2@test.de');
          await invitations.accept(1, sender);
          list = await invitations.list();
        });

        it('should contain the accepted invitation', () => {
          expect(list).toHaveLength(1);
          expect(list[0].id).toEqual(stored.id);
        });

        it('should change invitation status to ACCEPTED', () => {
          expect(list[0].status).toEqual('ACCEPTED');
        });

        it('should dispatch INVITATION_ACCEPTED event', () => {
          expect(events[2].type).toEqual('INVITATION_ACCEPTED');
          expect(events[2].payload).toEqual(acceptedAdminInvitation);
        });
      });

      describe('when declining an invitation', () => {
        let list, stored;

        beforeEach(async () => {
          stored = await invitations.send(invitation);
          await invitations.accept(1, 'test2@test.de');
          await invitations.decline(1, sender);
          list = await invitations.list();
        });

        it('should contain the declined invitation', () => {
          expect(list).toHaveLength(1);
          expect(list[0].id).toEqual(stored.id);
        });

        it('should change invitation status to DECLINED', () => {
          expect(list[0].status).toEqual('DECLINED');
        });

        it('should dispatch INVITATION_DECLINED event', () => {
          expect(events[2].type).toEqual('INVITATION_DECLINED');
          expect(events[2].payload).toEqual(declinedAdminInvitation);
        });
      });
    });
    describe('and invitation has already been accepted', () => {
      const sender = 'test2@test.de';


      describe('when declining an invitation', () => {
        let list, stored;

        beforeEach(async () => {
          stored = await invitations.send(invitation);
          await invitations.accept(1, 'test2@test.de');
          await invitations.accept(1, 'test@test.de');
          expect((await invitations.list())[0].status).toEqual('ACCEPTED');
          await invitations.decline(1, sender);
          list = await invitations.list();
        });

        it('should contain the accepted invitation', () => {
          expect(list).toHaveLength(1);
          expect(list[0].id).toEqual(stored.id);
        });

        it('should not change invitation status', () => {
          expect(list[0].status).toEqual('ACCEPTED');
        });

        it('should not dispatch an event after invitation was accepted', () => {
          expect(events[2].type).toEqual('INVITATION_ACCEPTED');
          expect(events[3]).toBeUndefined();
        });
      });
    });
  });
});