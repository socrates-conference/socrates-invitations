// @flow

import stream from 'stream';
import type {Logger} from 'server-commons/src/logger';
import type {SoCraTesEvent} from 'server-commons/src/streams/socratesEvent';
import Invitations from './invitations';
import type {Invitation} from './invitations';

export default class InvitationMessageSink extends stream.Writable {
  _logger: Logger;
  _invitations: Invitations;

  constructor(invitations: Invitations, logger: Logger) {
    super({objectMode: true});
    this._invitations = invitations;
    this._logger = logger;
    this._initEventHandlers();
  }

  _initEventHandlers(): void {
    this.on('error', err => {
      this._logger.error('Error while using InvitationMessageSink stream', err);
    });

    this.on('close', () => {
      this._logger.info('InvitationMessageSink stream was closed.');
    });

    this.on('finish', () => {
      this._logger.info('InvitationMessageSink stream finished.');
    });
  }

  //noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols
  // $FlowFixMe Type definition is broken: return type should be void, not boolean
  async _write(chunk: any, encoding: string, callback: (err?: Error, data?: any) => void): void {
    await this._receiveMessage(chunk);
    callback();
  }

  async _receiveMessage(event: SoCraTesEvent): Promise<?Invitation> {
    if (event.event === 'ACCEPT_INVITATION') {
      return this._invitations.accept(event.value.id, event.value.sender);
    } else if (event.event === 'DECLINE_INVITATION') {
      return this._invitations.decline(event.value.id, event.value.sender);
    } else if (event.event === 'SEND_INVITATION') {
      return this._invitations.send(event.value);
    }
    this._logger.error('Unrecognized message: %o', event);
  }
}