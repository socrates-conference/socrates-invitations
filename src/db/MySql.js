// @flow

import type {Config} from 'server-commons/src/ConfigType';
import * as mysql from 'mysql';

export type QueryResult = {
  lastInsertedId: number,
  numberOfRowsAffected: number
}

export default class MySql {

  config: Config;
  isInitialized: boolean;
  pool: mysql.Pool;

  constructor(config: Config) {
    this.config = config;
    this.isInitialized = false;
  }

  initialize(): void {
    if (!this.isInitialized && !this.pool) {
      this.isInitialized = true;
      if(!this.config.database) {
        throw new Error('FATAL: Database not configured properly. Please add a database property to config.js.');
      }
      const options: mysql.PoolOptions = {
        connectionLimit: 100, //important
        host: this.config.database.host,
        port: this.config.database.port,
        user: this.config.database.user,
        password: this.config.database.password,
        database: this.config.database.name,
        debug: this.config.database.debug
      };
      this.pool = mysql.createPool(options);
    }
  }

  select(statement: string): Promise<[]> {
    return new Promise((resolve, reject) => {
      if (!this.pool) {
        reject(new Error('MySql not initialized'));
      }
      this.pool.getConnection((connectionError, connection) => {
        if (connection) {
          connection.query(statement, (queryError, rows) => {
            if (connection) {
              connection.release();
            }
            if (!queryError) {
              resolve(rows);
            } else {
              reject(queryError);
            }
          });
        } else {
          reject(connectionError);
        }
      });
    });
  }

  selectWithParams(statement: string, values: Array<mixed>): Promise<Array<Object>> {
    return new Promise((resolve, reject) => {
      if (!this.pool) {
        reject(new Error('MySql not initialized'));
      }
      this.pool.getConnection((connectionError, connection) => {
        if (connection) {
          connection.query(statement, values, (queryError, rows) => {
            if (connection) {
              connection.release();
            }
            if (!queryError) {
              resolve(rows);
            } else {
              reject(queryError);
            }
          });
        } else {
          reject(connectionError);
        }
      });
    });
  }

  insert(statement: string, parameter: Array<mixed>): Promise<QueryResult> {
    return new Promise((resolve, reject) => {
      if (!this.pool) {
        reject(new Error('MySql not initialized'));
      }
      this.pool.getConnection((connectionError, connection) => {
        if (connection) {
          connection.query(statement, parameter, (queryError, result) => {
            if (connection) {
              connection.release();
            }
            if (!queryError) {
              const lastInsertedId = typeof result.insertId === 'number' ? result.insertId : 0;
              const numberOfRowsAffected = typeof result.affectedRows === 'number' ? result.affectedRows : 0;
              resolve({numberOfRowsAffected, lastInsertedId});
            } else {
              reject(queryError);
            }
          });
        } else {
          reject(connectionError);
        }
      });
    });
  }

  update(statement: string, parameter: Array<mixed>): Promise<QueryResult> {
    return new Promise((resolve, reject) => {
      if (!this.pool) {
        reject(new Error('MySql not initialized'));
      }
      this.pool.getConnection((connectionError, connection) => {
        if (connection) {
          connection.query(statement, parameter, (queryError, result) => {
            if (connection) {
              connection.release();
            }
            if (!queryError) {
              const affectedRows = typeof result.affectedRows === 'number' ? result.affectedRows : 0;
              resolve({numberOfRowsAffected: affectedRows, lastInsertedId: 0});
            } else {
              reject(queryError);
            }
          });
        } else {
          reject(connectionError);
        }
      });
    });
  }

  delete(statement: string, parameter: Array<mixed>): Promise<QueryResult> {
    return new Promise((resolve, reject) => {
      if (!this.pool) {
        reject(new Error('MySql not initialized'));
      }
      this.pool.getConnection((connectionError, connection) => {
        if (connection) {
          connection.query(statement, parameter, (queryError, result) => {
            if (connection) {
              connection.release();
            }
            if (!queryError) {
              const affectedRows = typeof result.affectedRows === 'number' ? result.affectedRows : 0;
              resolve({numberOfRowsAffected: affectedRows, lastInsertedId: 0});
            } else {
              reject(queryError);
            }
          });
        } else {
          reject(connectionError);
        }
      });
    });
  }
}

