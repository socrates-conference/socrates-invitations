// @flow

import type Config from '../config';
import rc from 'rc';
import Connection from 'mysql';

export default class SqlTestHelper {
  testConfig: Config = rc('socrates-server-test', {
    'environment': 'dev',
    'database': {
      'host': '',
      'port': 0,
      'name': '',
      'user': '',
      'password': ''
    },
    'server': {
      'port': 4444
    }
  });

  createPoolMock = (theConnection: typeof Connection) => ({
    getConnection: (callback: (error: ?Error, connection?: typeof Connection) => *) => {
      callback(null, theConnection);
    }
  });


}

