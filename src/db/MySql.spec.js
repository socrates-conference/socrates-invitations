// @flow

import MySql from './MySql';
import MockConnection from './MockConnection';

jest.mock('mysql');
import * as mysql from 'mysql';
import TestHelper from './SqlTestHelper';

const testHelper = new TestHelper();
let sqlHandler: MySql;

describe('MySql', () => {
  beforeEach(() => {
    sqlHandler = new MySql(testHelper.testConfig);
  });

  it('initializes only once', () => {
    const spyPool = jest.fn(() => {
    });
    mysql.createPool.mockImplementation(() => spyPool());
    sqlHandler.initialize();
    sqlHandler.initialize();
    expect(spyPool.mock.calls.length).toBe(1);
  });

  describe('can execute', () => {
    let connection;
    beforeEach(() => {
      connection = new MockConnection();
      mysql.createPool.mockImplementation(() => testHelper.createPoolMock(connection));
      sqlHandler.initialize();
    });

    afterEach(() => {
      mysql.createPool.mockReset();
    });

    it('select command', (done) => {
      sqlHandler.initialize();
      sqlHandler.select('SELECT * FROM person')
        .then((data) => {
          expect(data.length).toBe(0);
          done();
        });
    });

    it('select command with parameter', (done) => {
      connection.queryReturnValue = [];
      sqlHandler.selectWithParams('SELECT * FROM person WHERE email = ?', ['valid@email.de'])
        .then((data) => {
          expect(data.length).toBe(0);
          done();
        });
    });

    it('can execute insert', (done) => {
      connection.queryReturnValue = {affectedRows: 1, insertId: 4711};
      mysql.createPool.mockImplementation(() => testHelper.createPoolMock(connection));
      sqlHandler.initialize();
      sqlHandler.insert('INSERT INTO person(email) VALUES(?)', [1])
        .then((result) => {
          expect(result.numberOfRowsAffected).toBe(1);
          expect(result.lastInsertedId).toBe(4711);
          done();
        });
    });

    it('can execute delete', (done) => {
      connection.queryReturnValue = {affectedRows: 1};
      sqlHandler.delete('DELETE FROM person WHERE email = ?', [1])
        .then((result) => {
          expect(result.numberOfRowsAffected).toBe(1);
          done();
        });
    });

    it('can execute update', (done) => {
      connection.queryReturnValue = {affectedRows: 1};
      sqlHandler.update('UPDATE person SET email=\'value\' WHERE email = ?', [1])
        .then((result) => {
          expect(result.numberOfRowsAffected).toBe(1);
          done();
        });
    });

  });

  describe('when cannot execute', () => {
    let connection;
    let error;

    beforeEach(() => {
      connection = new MockConnection();
      error = new Error('query execution failure');
      connection.error = error;
      mysql.createPool.mockImplementation(() => testHelper.createPoolMock(connection));
      sqlHandler.initialize();
    });

    afterEach(() => {
      mysql.createPool.mockReset();
    });
    it('select query it throws error', (done) => {
      sqlHandler.select('SELECT * FROM person')
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });

    it('select query with parameter it throws error', (done) => {
      sqlHandler.selectWithParams('SELECT * FROM person WHERE email = ?', ['valid@email.de'])
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });

    it('insert query it throws error', (done) => {
      sqlHandler.insert('INSERT INTO person(email) VALUES(?)', [1])
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });
    it('delete query it throws error', (done) => {
      sqlHandler.delete('DELETE FROM person WHERE email = ?', [1])
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });
    it('update query it throws error', (done) => {
      sqlHandler.update('UPDATE person SET email=\'value\' WHERE email = ?', [1])
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });
  });

  describe('when cannot connect to database', () => {
    let error;
    beforeEach(() => {
      error = new Error('connection failure\'');
      const failingPoolMock = () => ({
        getConnection: (callback) => {
          callback(error, null);
        }
      });
      mysql.createPool.mockImplementation(() => failingPoolMock());
      sqlHandler.initialize();
    });

    afterEach(() => {
      mysql.createPool.mockReset();
    });

    it('on select it throws error', (done) => {
      sqlHandler.select('SELECT * FROM person')
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });

    it('on selectWithParams it throws error', (done) => {
      sqlHandler.selectWithParams('SELECT * FROM person WHERE email = ?', ['valid@email.de'])
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });

    it('on insert it throws error', (done) => {
      sqlHandler.insert('INSERT INTO person(email) VALUES(?)', [1])
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });

    it('on delete it throws error', (done) => {
      sqlHandler.delete('DELETE FROM person WHERE email = ?', [1])
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });
    it('on update it throws error', (done) => {
      sqlHandler.update('UPDATE person SET email=\'value\' WHERE email = ?', [1])
        .catch((err) => {
          expect(err.message).toEqual(error.message);
          done();
        });
    });
  });

  describe('when not initialized', () => {
    let spyPool;
    beforeEach(() => {
      spyPool = jest.fn(() => {
      });
      mysql.createPool.mockImplementation(() => spyPool());
    });

    afterEach(() => {
      mysql.createPool.mockReset();
    });

    it('throws error on selectWithParams execution', (done) => {
      expect(spyPool.mock.calls.length).toBe(0);
      sqlHandler.selectWithParams('SELECT * FROM person WHERE email = ?', ['valid@email.de'])
        .catch((err) => {
          expect(err.message).toEqual('MySql not initialized');
          done();
        });
    });

    it('throws error on insert execution', (done) => {
      expect(spyPool.mock.calls.length).toBe(0);
      sqlHandler.insert('INSERT INTO person(email) VALUES(?)', [1])
        .catch((err) => {
          expect(err.message).toEqual('MySql not initialized');
          done();
        });
    });

    it('throws error on select execution', (done) => {
      expect(spyPool.mock.calls.length).toBe(0);
      sqlHandler.select('SELECT * FROM person')
        .catch((err) => {
          expect(err.message).toEqual('MySql not initialized');
          done();
        });
    });

    it('throws error on delete execution', (done) => {
      expect(spyPool.mock.calls.length).toBe(0);
      sqlHandler.delete('DELETE FROM person WHERE email = ?', [1])
        .catch((err) => {
          expect(err.message).toEqual('MySql not initialized');
          done();
        });
    });
    it('throws error on update execution', (done) => {
      expect(spyPool.mock.calls.length).toBe(0);
      sqlHandler.update('UPDATE person SET email=\'value\' WHERE email = ?', [1])
        .catch((err) => {
          expect(err.message).toEqual('MySql not initialized');
          done();
        });
    });
  });
});