// @flow

import type {Config} from 'server-commons/src/ConfigType';

export type InvitationsConfig = Config & {
  app: {
    name: string,
    logLevel: string
  },
};

