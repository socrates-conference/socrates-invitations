// @flow

import rc from 'rc';
import type {InvitationsConfig} from './ConfigType';

// add your own config in .socrates-invitationsrc file and don't add it to git if you want to keep your secrets.

const config: InvitationsConfig = {
  app: {
    name:'socrates-invitations',
    logLevel: 'debug'
  },
  environment: 'dev',
  jwtSecret: '$lsRTf!gksTRcDWs',
  database: {
    host: '',
    port: 0,
    name: '',
    user: '',
    password: ',',
    debug: false
  },
  server: {
    port: 4444
  },
  kafka: {
    host: 'zookeeper',
    port: 2181,
    topics: ['socrates-events']
  }
};
export default rc(config.app.name, config);