// @flow

import express from 'express';
import type {Middleware} from 'server-commons/src/middleware';
import type {EventDispatcher} from 'server-commons/src/domain/eventDispatcher';
import Invitables from '../invitables';
import InvitablesApiVersion1 from './invitablesApiVersion1';

export default function createInvitablesServer(invitables: Invitables, dispatcher: EventDispatcher): Middleware {
  return (app: express.Application) => app.use('/api/v1/',
    new InvitablesApiVersion1(invitables, dispatcher));
}