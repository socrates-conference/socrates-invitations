// @flow

import * as express from 'express';
import Invitables from '../invitables';
import {tryToProcess} from 'server-commons';
import type {EventDispatcher} from 'server-commons/src/domain/eventDispatcher';

export default class InvitablesApiVersion1 extends express.Router {
  _invitables: Invitables;
  _dispatcher: EventDispatcher;

  constructor(invitables: Invitables, dispatcher: EventDispatcher) {
    super();
    this._invitables = invitables;
    this._dispatcher = dispatcher;
    this._initRoutes();
  }

  _initRoutes = () => {

    this.get('/invitables', async (req: express.Request, res: express.Response) => {
      const invitables = await this._invitables.list();
      res.status(200).send(invitables).end();
    });
    this.post('/invitables', async (req: express.Request, res: express.Response) => {
      await tryToProcess(req, res, person => {
        this._dispatcher.dispatchEvent({type: 'ALLOW_INVITATIONS', payload: person});
        return Promise.resolve();
      });
    });
    this.delete('/invitables', async (req: express.Request, res: express.Response) => {
      await tryToProcess(req, res, person => {
        this._dispatcher.dispatchEvent({type: 'DENY_INVITATIONS', payload: person});
        return Promise.resolve();
      });
    });
  };
}


