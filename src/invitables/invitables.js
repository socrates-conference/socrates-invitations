// @flow

import type {EventDispatcher} from 'server-commons';
import {Aggregate} from 'server-commons';
import type {Logger} from 'server-commons/src/logger';
import {InvitablesRepository} from './invitablesRepository';

export type Invitable = {
  id: number,
  fullName: string,
  gender: string,
  age?: number,
  email: string
}

export default class Invitables extends Aggregate {
  _logger: Logger;
  _repository: InvitablesRepository;

  constructor(repository: InvitablesRepository, dispatcher: EventDispatcher, logger: Logger) {
    super(dispatcher);
    this._repository = repository;
    this._logger = logger;
  }

  async allow(person: Invitable) {
    await this._repository.createOrUpdate(person);
  }

  async deny(person: Invitable) {
    await this._repository.delete(person);
  }

  async list(): Promise<Array<Invitable>> {
    return await this._repository.readAll();
  }

}