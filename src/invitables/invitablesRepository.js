//@flow

import type {Invitable} from './invitables';
import MySql from '../db/MySql';

type InsertResult = {error: Error} | {numberOfRowsAffected: number, lastInsertedId: number}
type UpdateResult = {error: Error} | {numberOfRowsAffected: number}

export interface InvitablesRepository {
  createOrUpdate(invitable: Invitable): Promise<Invitable>;

  delete(invitable: Invitable): Promise<void>;

  readAll(): Promise<Array<Invitable>>;
}

export default class InvitablesMySqlRepository implements InvitablesRepository {
  _mysql: MySql;

  constructor(mysql: MySql) {
    this._mysql = mysql;
  }

  async createOrUpdate(invitable: Invitable): Promise<Invitable> {
    const existing: ?Invitable = await this.findByEmail(invitable.email);
    if (existing) {
      return await this._update(invitable, existing);
    } else {
      return await this._create(invitable);
    }
  }

  async _create(invitable: Invitable) {
    const insertStatement = 'INSERT INTO invitables (full_name, gender, age, email) VALUES (?,?,?,?)';
    const {fullName, gender, age, email} = invitable;
    const queryResult: InsertResult = await this._mysql
      .insert(insertStatement, [fullName, gender, age, email])
      .catch(error => ({error}));

    if (queryResult.error) {
      throw new Error(queryResult.error);
    }

    const id: number = queryResult.lastInsertedId;
    return {...invitable, id};
  }

  async _update(update: Invitable, existing: Invitable) {
    const updateStatement = 'UPDATE invitables SET full_name=?, gender=?, age=? WHERE id=?';
    const combined = {...existing, fullName: update.fullName, gender: update.gender, age: update.age};
    const {fullName, gender, age, id} = combined; // email is always identical
    const queryResult: UpdateResult = await this._mysql
      .update(updateStatement, [fullName, gender, age, id])
      .catch(error => ({error}));

    if (queryResult.error) {
      throw new Error(queryResult.error);
    } else if (queryResult.numberOfRowsAffected < 1) {
      throw new Error('Error: Update did not affect any existing database entries, but should have: ' +
        JSON.stringify(update));
    }

    return {...combined};
  }

  async delete(invitable: Invitable): Promise<void> {
    const deleteStatement = 'DELETE FROM invitables WHERE id=?';
    const {id} = invitable;
    const deleteResult: InsertResult = await this._mysql
      .delete(deleteStatement, [id])
      .catch(error => ({error}));

    if (deleteResult.error) {
      throw new Error(deleteResult.error);
    } else if (deleteResult.numberOfRowsAffected < 1) {
      console.log('Tried to delete a non-existing Invitable: %d. Ignoring request.', id);
    }
  }

  async readAll(): Promise<Array<Invitable>> {
    const queryStatement = 'SELECT id, full_name AS fullName, gender, age, email FROM invitables';
    return await this._mysql.select(queryStatement);
  }

  async findByEmail(email: string): Promise<?Invitable> {
    const queryStatement = 'SELECT id, full_name AS fullName, gender, age, email FROM invitables WHERE email=?';
    const result = await this._mysql.selectWithParams(queryStatement, [email]);
    return result.length > 0 ? result[0] : undefined;
  }
}

export class InvitablesTestRepository implements InvitablesRepository {
  _invitables: Map<number, Invitable>;
  _count: number;

  constructor() {
    this._invitables = new Map();
    this._count = 0;
  }

  async createOrUpdate(invitable: Invitable): Promise<Invitable> {
    const existing: ?Invitable = [...this._invitables.values()].find(value => value.email === invitable.email);
    if (existing) {
      return {...existing, fullName: invitable.fullName, gender: invitable.gender, age: invitable.age};
    } else {
      const id = ++this._count;
      this._invitables.set(id, invitable);
      return {...invitable, id};
    }
  }

  async delete(invitable: Invitable) {
    this._invitables.delete(invitable.id);
  }

  async readAll() {
    return [...this._invitables.values()];
  }

}