// @flow

import Invitables from './invitables';
import {DomainEventDispatcher, testLogger} from 'server-commons';
import {InvitablesTestRepository} from './invitablesRepository';

const dispatcher = new DomainEventDispatcher();

describe('Invitables:', () => {
  let invitables: Invitables;

  beforeEach(() => {

    invitables = new Invitables(new InvitablesTestRepository(), dispatcher, testLogger);
  });

  describe('when no one has allowed invitations', () => {
    it('should return empty list', async () => {
      expect(await invitables.list()).toHaveLength(0);
    });
  });

  describe('when someone allows invitation', () => {
    const person = {fullName: 'tester', email: 'tester@test.de', id: 1, gender: 'female', age: 42};

    describe('and they\'re not already on the list', () => {
      it('should return list of one', async () => {
        await invitables.allow(person);
        const list = await invitables.list();
        expect(list).toHaveLength(1);
        expect(list[0]).toEqual(person);
      });
    });
    describe('and they\'re already on the list', () => {
      beforeEach(async () => {
        await invitables.allow(person);
      });
      it('should return list of one', async () => {
        await invitables.allow(person);
        const list = await invitables.list();
        expect(list).toHaveLength(1);
        expect(list[0]).toEqual(person);
      });
    });

  });
  describe('when someone denies invitations', () => {
    const person = {fullName: 'tester', email: 'tester@test.de', id: 1, gender: 'female', age: 42};
    const otherPerson = {fullName: 'tester2', email: 'tester2@test.de', id: 2, gender: 'male', age: 23};

    beforeEach(async () => {
      await invitables.allow(person);
    });

    describe('and they\'re on the list', () => {
      beforeEach(async () => {
        const personIdenticalToOriginalPerson = {
          fullName: 'tester',
          email: 'tester@test.de',
          id: 1,
          gender: 'female',
          age: 42
        };
        await invitables.deny(personIdenticalToOriginalPerson);
      });
      it('should return empty list', async () => {
        expect(await invitables.list()).toHaveLength(0);
      });
    });

    describe('and they\'re not on the list', () => {
      beforeEach(async () => {
        await invitables.deny(otherPerson);
      });
      it('should return unchanged list', async () => {
        const list = await invitables.list();
        expect(list).toHaveLength(1);
        expect(list[0]).toEqual(person);
      });
    });
  });
});