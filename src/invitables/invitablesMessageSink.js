// @flow

import stream from 'stream';
import type {Logger} from 'server-commons/src/logger';
import type {SoCraTesEvent} from 'server-commons/src/streams/socratesEvent';
import Invitables from './invitables';

export default class InvitablesMessageSink extends stream.Writable {
  _logger: Logger;
  _invitables: Invitables;

  constructor(invitables: Invitables, logger: Logger) {
    super({objectMode: true});
    this._logger = logger;
    this._invitables = invitables;
    this._initEventHandlers();
  }

  _initEventHandlers(): void {
    this.on('error', err => {
      this._logger.error('Error while using InvitablesMessageSink stream', err);
    });

    this.on('close', () => {
      this._logger.info('InvitablesMessageSink stream was closed.');
    });

    this.on('finish', () => {
      this._logger.info('InvitablesMessageSink stream finished.');
    });
  }

  //noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols
  // $FlowFixMe Type definition is broken: return type should be void, not boolean
  async _write(chunk: any, encoding: string, callback: (err?: Error, data?: any) => void): void {
    await this._receiveMessage(chunk);
    callback();
  }

  async _receiveMessage(event: SoCraTesEvent): Promise<void> {
    if (event.event === 'ALLOW_INVITATIONS') {
      this._invitables.allow(event.value);
    } else if (event.event === 'DENY_INVITATIONS') {
      this._invitables.deny(event.value);
    }
  }
}