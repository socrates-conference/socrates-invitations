// @flow

import bunyan from 'bunyan';
import {EventPublisher, EventStream, EventTracingStream} from 'socrates-event-stream';
import config from './config';
import {
  App,
  bodyParsers,
  createRootServer,
  defaultHeaders,
  DomainEventDispatcher,
  EventBus,
  MessageFilter,
  UniqueMessagesFilter
} from 'server-commons';
import MySql from './db/MySql';
import InvitationMessageSink from './invitations/invitationMessageSink';
import Invitations from './invitations/invitations';
import Invitables from './invitables/invitables';
import createInvitablesServer from './invitables/routes/invitablesServer';
import createInvitationsServer from './invitations/routes/invitationsServer';
import InvitablesMessageSink from './invitables/invitablesMessageSink';
import InvitablesMySqlRepository from './invitables/invitablesRepository';
import InvitationsMySqlRepository from './invitations/invitationsRepository';

const logger = bunyan.createLogger({
  name: config.app.name,
  level: config.app.logLevel
});
logger.info('Starting: ', config.app.name);

const dispatcher = new DomainEventDispatcher();
const sqlHandler = new MySql(config);
sqlHandler.initialize();
const invitables = new Invitables(new InvitablesMySqlRepository(sqlHandler), dispatcher, logger);

const invitations = new Invitations(new InvitationsMySqlRepository(sqlHandler), dispatcher, logger);


initExpressApp();
initEventStream();

// eslint-disable-next-line no-unused-vars
const eventbus = initEventBus();

function initExpressApp() {
  logger.info('Setting up Express server.');
  const app = new App(config, logger);
  app.applyMiddleware(defaultHeaders);
  app.applyMiddleware(bodyParsers);
  app.applyMiddleware(createInvitationsServer(invitations, dispatcher));
  app.applyMiddleware(createInvitablesServer(invitables, dispatcher));
  app.applyMiddleware(createRootServer());
  app.run();
}

function initEventStream() {
  logger.info('Setting up event stream via Kafka.');
  const topics = config.kafka.topics.map(topic => ({topic}));
  const read = new EventStream(topics, config.kafka.host, config.kafka.port, config.app.name, logger);
  read.on('error', err => console.error(err));
  read.startConsuming();

  const uniqueStream = read
    .pipe(new UniqueMessagesFilter(logger));

  uniqueStream
    .pipe(new MessageFilter(['ACCEPT_INVITATION', 'DECLINE_INVITATION', 'SEND_INVITATION'], logger))
    .pipe(new EventTracingStream(logger))
    .pipe(new InvitationMessageSink(invitations, logger));

  uniqueStream
    .pipe(new MessageFilter(['ALLOW_INVITATIONS', 'DENY_INVITATIONS'], logger))
    .pipe(new EventTracingStream(logger))
    .pipe(new InvitablesMessageSink(invitables, logger));
}

function initEventBus() {
  logger.info('Initializing event bus.');
  const kafka = new EventPublisher('socrates-events', config.kafka.host, `${config.kafka.port}`, logger);
  return new EventBus(dispatcher, kafka, logger);
}