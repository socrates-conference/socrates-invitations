// @flow

/* eslint-disable */
import type {InvitationsConfig} from '../../src/ConfigType';

const config: InvitationsConfig = {
  app: {
    name:'socrates-invitations',
    logLevel: 'trace'
  },
  'environment': 'dev',
  'database': {
    'host': 'socrates-db',
    'port': 3306,
    'name': 'socrates_db',
    'user': '%username%',
    'password': '%password%',
    'debug': false
  },
  'server': {
    'port': 4444
  },
  'kafka': {
    'host': 'zookeeper',
    'port': 2181,
    topics: ['socrates-events']
  }
};
export default config;